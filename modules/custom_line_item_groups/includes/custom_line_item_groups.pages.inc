<?php

/**
 * Displays the shopping cart form and associated information.
 * @see commerce_cart_view()
 * - changes embedded view to customized view
 */
function custom_line_item_groups_commerce_cart_view() {
  global $user;

  // First check to make sure we have a valid order.
  $order = commerce_cart_order_load($user->uid);
  if (!$order) {
    return theme('commerce_cart_empty_page');
  }

  $custom_line_item_groups = custom_line_item_groups_sorted();

  // TODO: Figure out how this can apply to showing multiple potentially-different views
  /*$enabled_views = views_get_enabled_views();
  if (!isset($enabled_views[$view_name])) {
    // Fallback to checkout
    drupal_goto('checkout');
  }*/

  $content = '';

  foreach ($custom_line_item_groups as $id => $group) {
    $content .= custom_line_item_groups_render($id, $order);
  }

  if (!empty($content)) {

    $content .= '<div class="instructions"><p>';
    $content .= t('You will be able to enter additional information about this request during the checkout process.');
    $content .= '</p></div>';

    $buttons_before = variable_get('custom_line_item_groups_show_buttons_before', FALSE);
    $buttons_after = variable_get('custom_line_item_groups_show_buttons_after', TRUE);

    if ($buttons_before || $buttons_after) {
      $buttons = custom_line_item_groups_get_cart_buttons();

      if ($buttons_before) {
        $content = drupal_render($buttons) . $content;
      }

      if ($buttons_after) {
        $content .= drupal_render($buttons);
      }
    }

    drupal_add_css(drupal_get_path('module', 'commerce_cart') . '/theme/commerce_cart.theme.css');
  } else {
    $content = theme('commerce_cart_empty_page');
  }

  return $content;
}

function custom_line_item_groups_get_cart_buttons() {
  return drupal_get_form('custom_line_item_groups_checkout_form');
}

function custom_line_item_groups_render($group_id, $order) {
  $group = custom_line_item_groups_get_group($group_id);

  if (!empty($group['view_name'])) {
    $view_name = $group['view_name'];
  } else {
    $view_name = variable_get('custom_line_item_groups_view_name', 'commerce_cart_form');
  }

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $included_types = custom_line_item_groups_get_included_types($group_id);

  $view = views_get_view($view_name, TRUE);

  if (!array_key_exists($group_id, $view->display)) {
    $view->display[$group_id] = $view->display['default'];
    $view->display[$group_id]->id = $group_id;
  }


  $view->display[$group_id]->display_options['filters']['type'] = custom_line_item_groups_get_filter($included_types);

  if (!$group['show_qty']) {
    unset($view->display[$group_id]->display_options['fields']['edit_quantity']);
  }

  if (!$group['show_price']) {
    unset($view->display[$group_id]->display_options['fields']['commerce_unit_price']);
    unset($view->display[$group_id]->display_options['fields']['commerce_total']);
    unset($view->display[$group_id]->display_options['footer']['line_item_summary']);
  }

  $content = '';
  if (commerce_line_items_quantity($wrapper->commerce_line_items, $included_types) > 0) {
    if ($group['show_group_title']) {
      $content .= '<h2>'.$group['name'].'</h2>';
    }
    $content .= custom_line_item_groups_embed_view($view, $group_id, array($order->order_id), 'cart');
  }

  return $content;
}

function custom_line_item_groups_get_included_types($group_id) {
  $custom_line_item_types = custom_line_item_types();

  $included_types = ($group_id == 'default') ? commerce_product_line_item_types() : array();

  foreach ($custom_line_item_types as $type_id => $type) {
    if ($group_id == 'default') {
      if ($type['group'] !== 'default') {
        unset($included_types[array_search($type['commerce_type'], $included_types)]);
      }
    } else {
      if ($type['group'] == $group_id) {
        $included_types[] = $type['commerce_type'];
      }
    }
  }

  return $included_types;
}

function custom_line_item_groups_embed_view($view, $display_id, $arguments, $override_url = '') {
  // Load the specified View.
  $view->set_display($display_id);

  // Set the specific arguments passed in.
  $view->set_arguments($arguments);

  // Override the view url, if an override was provided.
  if (!empty($override_url)) {
    $view->override_url = $override_url;
  }

  // Prepare and execute the View query.
  $view->pre_execute();
  $view->execute();

  // Return the rendered View.
  return $view->render();
}

function custom_line_item_groups_get_filter($types, $operation = 'include') {
  $values = array();

  foreach ($types as $type) {
    $values[$type] = $type;
  }

  return array (
    'id' => 'type',
    'table' => 'commerce_line_item',
    'field' => 'type',
    'relationship' => 'commerce_line_items_line_item_id',
    'group_type' => 'group',
    'ui_name' => '',
    'operator' => ($operation == 'include') ? 'in' : 'not in',
    'value' => $values,
    'group' => '1',
    'exposed' => false,
    'expose' =>
      array (
        'operator_id' => false,
        'label' => '',
        'description' => '',
        'use_operator' => false,
        'operator' => '',
        'identifier' => '',
        'required' => false,
        'remember' => false,
        'multiple' => false,
        'remember_roles' =>
          array (
            2 => 2,
          ),
        'reduce' => false,
      ),
    'is_grouped' => false,
    'group_info' =>
      array (
        'label' => '',
        'description' => '',
        'identifier' => '',
        'optional' => true,
        'widget' => 'select',
        'multiple' => false,
        'remember' => 0,
        'default_group' => 'All',
        'default_group_multiple' =>
          array (
          ),
        'group_items' =>
          array (
          ),
      ),
  );
}