<?php

/**
 * @file custom_line_item_types.api.php
 * Hooks provided by the Custom line item types module.
 */

/**
 * @addtogroup hooks
 * @{
 */
/**
 * Hook example for custom_line_item_types_info
 */
function hook_custom_line_item_types_info() {
  $custom_line_item_types['my_type'] = array(
    'name'      => 'My Type',
    'has_qty'   => false,
    'has_price' => false,
    'group'     => 'my_group',
  );

  return $custom_line_item_types;
}

/**
 * Hook example for custom_line_item_types_alter
 * @param $custom_line_item_types array All defined custom line item types for direct modification
 */
function hook_custom_line_item_types_alter(&$custom_line_item_types) {
  $custom_line_item_types['my_type']['has_price'] = true;
}

/**
 * Hook example for custom_line_item_groups_info
 */
function hook_custom_line_item_groups_info() {
  $custom_line_item_groups['my_group'] = array(
    'name'   => 'My Group',
    'weight' => 10,
  );

  return $custom_line_item_groups;
}

/**
 * @param $custom_line_item_groups array All defined custom line item groups for direct modification
 */
function hook_custom_line_item_groups_alter(&$custom_line_item_groups) {
  $custom_line_item_groups['my_group']['weight'] = 100;
}