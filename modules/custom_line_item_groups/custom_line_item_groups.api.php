<?php

/**
 * @file custom_line_item_groups.api.php
 * Hooks provided by the Custom line item types module.
 */

/**
 * @addtogroup hooks
 * @{
 */
/**
 * Hook example for custom_line_item_groups_info
 */
function hook_custom_line_item_groups_info() {
  $custom_line_item_groups['my_group'] = array(
    'name'       => 'My Group',
    'weight'     => 10,
    'show_price' => TRUE,
    'show_update_button' => FALSE,
    'show_qty'   => TRUE,
  );

  return $custom_line_item_groups;
}

/**
 * @param $custom_line_item_groups array All defined custom line item groups for direct modification
 */
function hook_custom_line_item_groups_alter(&$custom_line_item_groups) {
  $custom_line_item_groups['my_group']['weight'] = 100;
}