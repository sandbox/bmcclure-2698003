(function ($) {
    "use strict";

    function showLineItemVariationsForm(lineItemType, productId) {
        //$('#custom-line-item-types-variations-form-display-' + lineItemType + '-' + productId).css(css).fadeIn();

        $('#custom-line-item-types-variations-form-display-' + lineItemType + '-' + productId).reveal();
    }

    function setupLineItemMessageContainer() {
        var loadingImageContainer, messageContainer;

        loadingImageContainer = $('<div class="custom-line-item-loading"><span class="throbber"></span></div>');

        messageContainer = $('<div class="custom-line-item-message reveal-modal" id="custom-line-item-message">');

        messageContainer.appendTo('body').hide();
        loadingImageContainer.appendTo('body').hide();
    }

    function customLineItemTypesMessage(text) {
        var newText = text + '<a class="close-reveal-modal">&#215;</a>';

        $('#custom-line-item-message').html(newText).reveal({

            opened: function () {
                $('#custom-line-item-message').delay(3000).trigger('reveal:close');
            }
        });
    }

    $(document).ready(function () {
        $.fn.customLineItemTypesMessage = function (data) {
            customLineItemTypesMessage(data);
        };

        setupLineItemMessageContainer();

        //$('.custom-line-item-types-variations-form-display').appendTo('body');

        $(document).on('click', '.custom-line-item-types-links .custom-line-item-types-variations-form a', function (event) {
            var link = $(this),
                hrefParts,
                productId,
                lineItemType;

            hrefParts = link.attr('href').split('/');
            productId = hrefParts.pop();
            lineItemType = hrefParts.pop();

            $('#custom-line-item-types-variations-form-display-' + lineItemType + '-' + productId).appendTo('body');

            showLineItemVariationsForm(lineItemType, productId);

            return false;
        });

        $(document).on('click', '.custom-line-item-types-links .custom-line-item-types-single a', function (event) {
            var link = $(this);

            $('.custom-line-item-loading').css({position: "absolute", top: event.pageY, left: event.pageX}).show();

            $.ajax({
                type: "GET",
                url: link.attr('href'),
                complete: function (data) {
                    $('.custom-line-item-loading').hide();
                    customLineItemTypesMessage('Product has been added to your <a href="/cart">cart</a> successfully.', event.pageY, event.pageX);
                }
            });

            return false;
        });
    });
}(jQuery));