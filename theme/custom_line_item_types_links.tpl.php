
<?php // dpm(get_defined_vars()); ?>
<?php /*
<div class="custom-line-item-types-links">
<?php foreach ($types as $id => $type): ?>
  <?php $class = ($type['variations_form'] && $has_variations) ? ' class="custom-line-item-types-variations-form"' : ''; ?>
  <?php //$options = $type['variations_form'] ? array('attributes' => array('class' => array('colorbox', 'load')), 'query' => array('width' => 500, 'height' => 500, 'iframe' => 'true')) : array(); ?>
  <div id="custom_line_item_type_<?php echo $id; ?>_link"<?php echo $class; ?>>
    <?php echo l(t($type['add_to_cart_text']), "add-line-item/$id/$product_id"); ?>
  </div>
<?php endforeach; ?>
</div>
*/ ?>


<?php
  $classes = array();
  foreach ($types as $id => $type) {
    $cntr_class     = ($type['line_item_type']['variations_form'] && $type['has_variations']) ? ' class="custom-line-item-types-variations-form"' : ' class="custom-line-item-types-single"';
    $cntr_id_class  = ' id="custom_line_item_type_'.$id.'_link"' . $cntr_class;
    
    switch ($view_mode) {
      case 'teaser':
        $classes = explode(' ','tiny button micro');
        echo '<li '.$cntr_id_class .'>' . l(t($type['line_item_type']['add_to_cart_text']), "add-line-item/$id/$product_id" , array('attributes'=>array('class'=>$classes))) . '</li>';
        break;
      
      case 'full' :
      case 'default' :
        $classes = explode(' ','tiny button');
        echo '<span '.$cntr_id_class .'>' . l(t($type['line_item_type']['add_to_cart_text']), "add-line-item/$id/$product_id" , array('attributes'=>array('class'=>$classes))) . '</span>';
        break;

      
    }

  }

  foreach ($types as $id => $type) {
    if (!$type['has_variations']) {
      continue;
    }

    echo '<div class="custom-line-item-types-variations-form-display reveal-modal" id="custom-line-item-types-variations-form-display-'.$id.'-'.$product_id.'">'.$type['variations_form'].'<a class="close-reveal-modal">&#215;</a></div>';
  }
?>

